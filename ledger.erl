-module(ledger).
-export([start/0]).
-export([receiverLoop/1, listenerLoop/0, pending/2]).

-define(Dbg(Str),io:format("[DBG]~p:" ++ Str,[?FUNCTION_NAME])). 
-define(Dbg(Str,Args),io:format("[DBG]~p:" ++ Str,[?FUNCTION_NAME|Args])).


start()->
    register(ledgerReceiver, spawn(?MODULE, receiverLoop,[[]])),
    abroadcast:start(ledgerReceiver),
    
    register(listener, spawn(?MODULE, listenerLoop,[])),
    register(pendingHandler, spawn(?MODULE, pending, [[],[]])).



listenerLoop() ->
    receive
        {C, get, Requester} ->
            ?Dbg("Listener: Llego una get request de forma ~p ~n", [{C, get, Requester}]),
            
            abroadcast:send({get, Requester, C}),
            pendingHandler ! {addGet, {Requester, C}},
            listenerLoop();
        {C, append, Record, Requester} ->
            ?Dbg("Listener: Llego un append request de forma ~p ~n", [{C, append, Record, Requester}]),

            abroadcast:send({append, Requester, C, Record}),
            pendingHandler ! {addAppend, {Requester, C, Record}},
            listenerLoop()
    end.


pending(GetPending, AppendPending) ->
    receive
        {addGet, GetReq} ->
            ?Dbg("Agregada una get request ~n"),
            pending([GetReq | GetPending], AppendPending);
        {addAppend, AppendReq} ->
            ?Dbg("Agregado un append request ~n"),
            pending(GetPending, [AppendReq| AppendPending]);
        
        {checkGet, GetReq, Process} ->
            ?Dbg("Llego un check get request ~n"),
            Process ! {checkGetRes, lists:member(GetReq, GetPending)},
            pending(GetPending, AppendPending);
        {checkAppend, AppendReq, Process} ->
            ?Dbg("Llego un checkAppend request ~n"),
            Process ! {checkAppendRes, lists:member(AppendReq, AppendPending)},
            pending(GetPending, AppendPending);

        {removeGet, GetReq} ->
            NewGetPending = [Elem || Elem <- GetPending, Elem =/= GetReq],
            ?Dbg("Se removio el elemento ~p ~n Status de la listas: ~p ~p ~n", [GetReq, NewGetPending, AppendPending]),
            pending(NewGetPending, AppendPending);
        {removeAppend, AppendReq} ->
            ?Dbg("Se removio el elemento ~p ~n", [AppendReq]),
            pending(GetPending, [X || X <- AppendPending, X =/= AppendReq])
    end.

handleGet(Ledger, Requester, C) ->
    pendingHandler ! {checkGet, {Requester, C}, self()},

    receive
        {checkGetRes, true} ->
            Requester ! {C, getRes, Ledger},
            pendingHandler ! {removeGet, {Requester, C}},
            ok;
        {checkGetRes, false} ->
            notModified
    end.

updatePending(Requester, C, Record) ->
    pendingHandler ! {checkAppend, {Requester, C, Record}, self()},

    receive
        {checkAppendRes, true} ->
            Requester ! {C, appendRes, ack},
            pendingHandler ! {removeAppend, {Requester, C, Record}};
        {checkAppendRes, false} ->
            ok
    end.

handleAppend(Ledger, Requester, C, Record) ->
    case lists:member(Record, Ledger) of
        false ->
            NewLedger = [Record | Ledger],
            updatePending(Requester, C, Record),
            NewLedger;
        true ->
            Ledger
    end.

receiverLoop(Ledger) ->
    receive
        {get, Requester, C} -> 
            Temp = handleGet(Ledger, Requester, C),
            ?Dbg("handleGet finalizo exitosamente con ~p ~n", [Temp]),
            receiverLoop(Ledger);
        {append, Requester, C, Record} ->
            Temp = handleAppend(Ledger, Requester, C, Record),
            ?Dbg("handleAppend finalizo, ledger nuevo: ~p ~n", [Ledger]),
            receiverLoop(Temp)
    end.