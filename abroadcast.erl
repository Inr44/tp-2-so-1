-module(abroadcast).
-export([start/0, start/1 ,stop/0]).
-export([receiverLoop/2, senderLoop/1, sequencer/2, deliver/0, deliver/1]).
-export([send/1]).
-export([sortingFun/2]). %Se exporta solamente para suprimir el warning que da erlang por no ser llamada directamente.

%-define(Dbg(Str),io:format("[DBG]~p:" ++ Str,[?FUNCTION_NAME])). 
%-define(Dbg(Str,Args),io:format("[DBG]~p:" ++ Str,[?FUNCTION_NAME|Args])).

-record(msg, {status, id, msg, sender, sn}).

start() ->
    register(receiver, spawn(?MODULE, receiverLoop,[[], 0])),
    register(sender, spawn(?MODULE, senderLoop,[0])),
    register(seq, spawn(?MODULE, sequencer,[0, 0])),
    register(deliver, spawn(?MODULE, deliver,[])).

start(Process) ->
    register(receiver, spawn(?MODULE, receiverLoop,[[], 0])),
    register(sender, spawn(?MODULE, senderLoop,[0])),
    register(seq, spawn(?MODULE, sequencer,[0, 0])),
    register(deliver, spawn(?MODULE, deliver,[Process])).    

stop() ->
    receiver ! fin,
    sender ! fin,
    seq ! fin,
    deliver ! fin,

    unregister(deliver),
    unregister(seq),
    unregister(sender),
    unregister(receiver).

send(Msg) ->
    sender ! {sendReq, Msg, self()},
    ok.

sendRequests(Id, Msg, Sender, BroadcasterList) ->
    lists:foreach(fun (X) -> {seq, X} ! {seqReq, self(), Id, Msg, Sender} end, BroadcasterList),
    seq ! {seqReq, self(), Id, Msg, Sender},
    ok.

waitForResponses(NodesAmount, Max) ->
    if
        NodesAmount > 0 ->
            %?Dbg("Sender: Esperando respuestas, nodos restantes: ~p ~n",[NodesAmount]),
            receive
                {seqResponse, Proposal, Node} ->
                    %?Dbg("Sender: Recibida una respuesta: ~p ~n",[Proposal]),
                    monitor_node(Node, false),
                    waitForResponses(NodesAmount-1, max(Max, Proposal));
                {nodedown, Node} ->
                    monitor_node(Node, false),
                    waitForResponses(NodesAmount-1, Max)
            end;
        true ->
            Max
    end.

checkProcesses(List, Node) ->
    case lists:member(seq, List) of
        true -> 
            monitor_node(Node, true),
            true;
        false ->
            false
    end.

filterFun(Node) ->
    Response = rpc:call(Node, erlang, registered, []),
    if
        is_list(Response) ->
            checkProcesses(Response, Node);
        true -> 
            false
    end.

getBroadcasters () ->
    lists:filter(fun filterFun/1, nodes()).

senderLoop(N) ->
    receive 
        {sendReq, Msg, Sender} ->
            %?Dbg("Sender: Recibido solicitud de enviar mensaje ~p ~n", [Msg]),
            
            NewId = {node(), N},

            BroadcasterList = getBroadcasters(),
            %?Dbg("Sender: Lista de broadcasters: ~p ~n",[BroadcasterList]),

            sendRequests(NewId, Msg, Sender, BroadcasterList),
            %?Dbg("Sender: Enviadas las requests ~n"),
            
            SeqNum = waitForResponses(length(BroadcasterList) + 1, 0),
            %?Dbg("Sender: Recibidas las repuestas, numero elegido ~p ~n", [SeqNum]),
            

            M = #msg{status = deliverable, id = NewId, msg = Msg, sender = Sender, sn = SeqNum},
            lists:foreach(fun (X) -> {receiver, X} ! M end, nodes()),
            receiver ! M,

            %?Dbg("Sender: Enviados los mensajes ~n"),
            senderLoop(N + 1)
    end.

sequencer(MaxSeen, MaxProposed) ->
    receive
        {seqReq, Node, Id, Msg, Sender} ->
            Proposal = (max(MaxSeen, MaxProposed) + 1),
            %?Dbg("Seq: Recibida una requests de ~p, contestando ~p ~n",[Node, Proposal]),
            
            receiver ! #msg{status = undeliverable, id = Id, msg = Msg, sender = Sender, sn = Proposal},
            
            Node ! {seqResponse, Proposal, node()},

            sequencer(MaxSeen, Proposal);
        {seen, SeqNum} ->
            %?Dbg("Seq:Recibido un update de seen a ~p ~n",[SeqNum]),
            sequencer(max(MaxSeen, SeqNum), MaxProposed)
    end.

deliver() ->
    receive
        fin -> exit(normal);
        M ->
            io:format("Deliver : ~p ~n", [M]),
            deliver()
    end.

deliver(Process) ->
    receive
        fin -> exit(normal);
        M ->
            Process ! M,
            deliver(Process)
    end.

updatePending(Pending, Data) ->
    [Data|lists:keydelete(Data#msg.id, #msg.id, Pending)].

sortingFun(Msg1, Msg2) ->
    {Msg1#msg.sn, Msg1#msg.id} =< {Msg2#msg.sn, Msg2#msg.id}.


receiverLoop(Pending, TO) ->
    receive
        Data when is_record(Data,msg) ->
            %?Dbg("Receiver: Recibido el msg ~p ~n",[Data]),
            
            if
                Data#msg.status == deliverable ->
                    seq ! {seen, Data#msg.sn},
                    %?Dbg("Receiver:enviado msg al sequencer ~n"),

                    Newpending = lists:sort(fun sortingFun/2, updatePending(Pending, Data));

                true ->
                    Newpending = lists:sort(fun sortingFun/2, [Data | Pending])
            end,
            
            receiverLoop(Newpending, 0)
    after TO ->
        receiverLoop(Pending)
    end.

receiverLoop([]) ->
    receiverLoop([], infinity);

receiverLoop([Head | Tail] = Pending) ->
    if
        Head#msg.status == deliverable ->
            deliver ! Head#msg.msg,
            receiverLoop(Tail, 0);
        true -> 
            receiverLoop(Pending, infinity)
    end.