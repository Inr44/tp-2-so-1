-module(client).

-export([start/1]).
-export([clientHandler/2]).
-export([get/0, ledgerAppend/1]).

-define(Dbg(Str),io:format("[DBG]~p:" ++ Str,[?FUNCTION_NAME])). 
-define(Dbg(Str,Args),io:format("[DBG]~p:" ++ Str,[?FUNCTION_NAME|Args])).

start(ServerList) ->
    register(handler, spawn(?MODULE, clientHandler, [ServerList, 0])).

flush() ->
    receive
        _ -> flush()
    after
         0 -> ok
    end.


get() ->
    handler ! {getReq, self()},
    receive
        {_, getRes, Ledger} ->
            flush(),
            Ledger
    end.

ledgerAppend(Record) ->
    handler ! {appendReq, self(), Record},
    ?Dbg("Enviado una appendReq ~n"),
    receive
        {_, appendRes, Ack} -> 
            ?Dbg("Llego el ack del append ~n"),
            flush(),
            Ack
    end.

clientHandler(ServerList, C) ->
    receive
        {getReq, Process} ->
            lists:foreach(fun (X) -> {listener, X} ! {C, get, Process} end, ServerList),
            clientHandler(ServerList, C + 1);
        {appendReq, Process, Record} ->
            lists:foreach(fun (X) -> {listener, X} ! {C, append, {{C, Process, node()}, Record}, Process} end, ServerList),
            clientHandler(ServerList, C + 1)
    end.
