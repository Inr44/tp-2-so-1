-module(dest).
-include("struct.hrl").
-export([start/0,stop/0]).
-export([receiverLoop/3, senderLoop/0, sequencer/2, deliver/0]).
-export([send/1]).


-define(Dbg(Str),io:format("[DBG]~p:" ++ Str,[?FUNCTION_NAME])). 
-define(Dbg(Str,Args),io:format("[DBG]~p:" ++ Str,[?FUNCTION_NAME|Args])).

start() ->
    register(receiver, spawn(?MODULE, receiverLoop,[1, dict:new(), 0])),
    register(sender, spawn(?MODULE, senderLoop,[])),
    register(seq, spawn(?MODULE, sequencer,[0, 0])),
    register(deliver, spawn(?MODULE, deliver,[])).

stop() ->
    receiver ! fin,
    deliver ! fin,
    unregister(deliver),
    unregister(receiver).

send(Msg) ->
    sender ! #send{msg = Msg, sender = self()},
    ok.

sendRequests() ->
    lists:foreach(fun (X) -> {seq, X} ! {seqReq, self()} end, nodes()),
    seq ! {seqReq, self()},
    ok.

waitForResponses(NodesAmount, Max) ->
    if
        NodesAmount > 0 ->
            ?Dbg("Sender: Esperando respuestas, nodos restantes: ~p ~n",[NodesAmount]),
            receive
                {seqResponse, Proposal} ->
                    ?Dbg("Sender: Recibida una respuesta: ~p ~n",[Proposal]),
                    waitForResponses(NodesAmount-1, max(Max, Proposal))
            end;
        true ->
            Max
    end.

senderLoop() ->
    receive 
        #send{msg = Msg, sender = Sender} ->
            ?Dbg("Sender: Recibido solicitud de enviar mensaje ~s ~n", [Msg]),
            
            sendRequests(),
            ?Dbg("Sender: Enviadas las requests ~n"),
            
            SeqNum = waitForResponses(length(nodes()) + 1, 0),
            ?Dbg("Sender: Recibidas las repuestas, numero elegido ~p ~n", [SeqNum]),
            
            M = #msg{msg = Msg, sender = Sender, sn = SeqNum},
            lists:foreach(fun (X) -> {receiver, X} ! M end, nodes()),
            receiver ! M,

            ?Dbg("Sender: Enviados los mensajes ~n"),
            senderLoop()
    end.

sequencer(MaxSeen, MaxProposed) ->
    receive
        {seqReq, Node} ->
            Proposal = (max(MaxSeen, MaxProposed) + 1),
            ?Dbg("Seq: Recibida una requests de ~p, contestando ~p ~n",[Node, Proposal]),
            Node ! {seqResponse, Proposal},
            sequencer(MaxSeen, Proposal);
        {seen, SeqNum} ->
            ?Dbg("Seq:Recibido un update de seen a ~p ~n",[SeqNum]),
            sequencer(max(MaxSeen, SeqNum), MaxProposed)
    end.

deliver() ->
    receive
        fin -> exit(normal);
        M ->
            io:format("Deliver : ~p ~n", [M]),
            deliver()
    end.


receiverLoop(NextMsg, Pending, TO) ->
    receive
        Data when is_record(Data,msg) ->
            ?Dbg("Receiver: Recibido el msg ~p ~n",[Data]),
            
            seq ! {seen, Data#msg.sn},
            ?Dbg("Receiver:enviado msg al sequencer ~n"),
            
            catch receiverLoop(NextMsg
                    , dict:append(Data#msg.sn, #send{msg = Data#msg.msg, sender = Data#msg.sender}, Pending)
                    , 0)
    after TO ->
            case dict:find(NextMsg, Pending) of
                {ok, Msg} ->
                    deliver ! Msg,
                    receiverLoop(NextMsg + 1, Pending, 0);
                error ->
                    receiverLoop(NextMsg, Pending, infinity)
            end
    end.
